// pushrocks scope
import * as smartpromise from '@pushrocks/smartpromise';
import * as events from 'events';

export { events, smartpromise };

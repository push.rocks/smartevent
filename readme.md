# @pushrocks/smartevent
handle events in smart ways

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@pushrocks/smartevent)
* [gitlab.com (source)](https://gitlab.com/pushrocks/smartevent)
* [github.com (source mirror)](https://github.com/pushrocks/smartevent)
* [docs (typedoc)](https://pushrocks.gitlab.io/smartevent/)

## Status for master
[![pipeline status](https://gitlab.com/pushrocks/smartevent/badges/master/pipeline.svg)](https://gitlab.com/pushrocks/smartevent/commits/master)
[![coverage report](https://gitlab.com/pushrocks/smartevent/badges/master/coverage.svg)](https://gitlab.com/pushrocks/smartevent/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@pushrocks/smartevent.svg)](https://www.npmjs.com/package/@pushrocks/smartevent)
[![Known Vulnerabilities](https://snyk.io/test/npm/@pushrocks/smartevent/badge.svg)](https://snyk.io/test/npm/@pushrocks/smartevent)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

Use intellisense for best in class intellisense.

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
